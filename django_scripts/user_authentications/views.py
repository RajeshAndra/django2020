from django.shortcuts import render

# Create your views here.
from django.contrib.auth import authenticate, login
from django.http import JsonResponse,HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

''' create user '''
@csrf_exempt
def create_users(request):
    user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')



'''  authenticate login   '''

@csrf_exempt
def authenticate_login(request):
    username = request.POST['username']   #form data
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
        return HttpResponse("successfully login!!!")
    else:
        # Return an 'invalid login' error message.
        return HttpResponse("login failed")
